package xjt.demo.dao.foundation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import xjt.demo.entity.foundation.DictionaryCategory;

/**
 * 字典类别数据访问
 * 
 * @author vincent
 *
 */
public interface DictionaryCategoryDao extends JpaRepository<DictionaryCategory, String>, JpaSpecificationExecutor<DictionaryCategory>{

}
