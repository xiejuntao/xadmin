package xjt.main;

import org.eclipse.jetty.server.Server;

import xjt.common.unit.JettyFactory;

public class LaunchJetty {
	/**
	 * 端口
	 */
	public static final int PORT = 80;
	/**
	 * 项目名称
	 */
	public static final String CONTEXT = "/xue";

	public static void main(String[] args) throws Exception {
		Server server = JettyFactory.buildNormalServer(PORT, CONTEXT);
		server.start();
	}
}
